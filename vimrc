set nocp ai si noet showmatch ic incsearch hidden
set guioptions-=T
set mouse=a
set number
set gfn=Noto\ Mono\ 14

" starts search with current highlights in visual mode
vnoremap <C-S> "ay/<C-R>=escape(@a,"\\/\'")<CR><CR>
" cont. search
nnoremap <C-S> n
inoremap <C-S> <C-O>n
inoremap <F2> <C-O>:w<CR>
noremap <F2> :w<CR>

inoremap <F3> <C-O>:NERDTreeToggle<CR>
noremap <F3> :NERDTreeToggle<CR>

inoremap <S-F3> <C-O>:Fern -drawer .<CR>
noremap <S-F3> :Fern -drawer .<CR>

inoremap <F4> <C-O>:only<CR>
noremap <F4> :only<CR>

noremap <C-Left> <C-W><Left>
noremap <C-Right> <C-W><Right>
noremap <C-Up> <C-W><Up>
noremap <C-Down> <C-W><Down>
inoremap <F7> <C-O>:set hlsearch!<CR>
noremap <F7> :set hlsearch!<CR>

call plug#begin('~/.vim/plugged')
Plug 'prabirshrestha/vim-lsp'
Plug 'mattn/vim-lsp-settings'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
Plug 'tpope/vim-fugitive'
Plug 'Bling/vim-airline'
" Plug 'kien/ctrlp.vim'
Plug 'bling/vim-airline'
Plug 'rust-lang/rust.vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'christianrondeau/vim-base64'

" Plug 'Shougo/denite.nvim'
" Plug 'vim-scripts/VOom'
Plug 'dracula/vim'
Plug 'zefei/vim-wintabs'
Plug 'zefei/vim-wintabs-powerline'
Plug 'github/copilot.vim'

Plug 'lambdalisue/fern.vim'

Plug 'pechorin/any-jump.vim'
call plug#end()

let g:airline_section_gutter="ascii:%b[%B]"
set laststatus=2

syntax enable
filetype plugin indent on
colorscheme dracula
packadd! xmledit
